﻿using CustomerClient.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomerClient.Controllers
{
    public class CustomerController : Controller
    {
        private Customer customer;
        private Order order;
        private Auction auction;
        public CustomerController()
        {
            customer = new Customer();
            order = new Order();
            auction = new Auction();
        }
        public ActionResult Index(int id)
        {
            if (Session["loginCustomer"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var dataaucByCusId = auction.GetListByCustId(id);
            var data = order.GetOdByCustID(id).OrderBy(x => x.Created);
            ViewBag.Order0 = data.Where(x => x.status == 0);
            ViewBag.AuctionAll = dataaucByCusId.OrderByDescending(x=>x.Created);
            ViewBag.StCount0 = dataaucByCusId.Where(x => x.Status == 0).Count();
            ViewBag.StCount1 = dataaucByCusId.Where(x => x.Status == 1).Count();
            ViewBag.StCount2 = dataaucByCusId.Where(x => x.Status == 2).Count();
            ViewBag.StCount3 = dataaucByCusId.Where(x => x.Status == 3).Count();

            return View();
        }

        public ActionResult PersonalProfile(int id)
        {
            ViewBag.profile = customer.GetCustById(id);
            return PartialView("_PersonalProfile");
        }

        public ActionResult MyOrder(int id,int? page)
        {
            if (Session["loginCustomer"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 20;
            var data = order.GetOdByCustID(id).OrderBy(x => x.Created);
            ViewBag.OrderAll = data.ToPagedList(_page, _pageSize);
            ViewBag.Order0 = data.Where(x => x.status == 0).ToPagedList(_page, _pageSize);
            ViewBag.Order1 = data.Where(x => x.status == 1).ToPagedList(_page, _pageSize);
            ViewBag.Order2 = data.Where(x => x.status == 2).ToPagedList(_page, _pageSize);
            ViewBag.Order3 = data.Where(x => x.status == 3).ToPagedList(_page, _pageSize);
            ViewBag.Order4 = data.Where(x => x.status == 4).ToPagedList(_page, _pageSize);

            ViewBag.StCount0 = data.Where(x => x.status == 0).Count();
            ViewBag.StCount1 = data.Where(x => x.status == 1).Count();
            ViewBag.StCount2 = data.Where(x => x.status == 2).Count();
            ViewBag.StCount3 = data.Where(x => x.status == 3).Count();


            return View();
        }

        public ActionResult UpdateOrder(int id)
        {
            ViewBag.OrderData = order.GetOrderById(id);
            return PartialView("_UpdateOrder");
        }

        [HttpPost]
        public ActionResult UpdateOrder(FormCollection form)
        {
            Order data = order.GetById(int.Parse(form["id"]));
            try
            {
                data.Email = form["email"];
                data.Receiver = form["receiver"];
                data.Phone = form["phone"];
                data.Note = form["note"];
                data.Payment = int.Parse(form["payment"]);
                data.status = 1;
                order.Edit(data);
                return RedirectToAction("MyOrder", new { id = data.CustomerID });
            }
            catch { }
            return RedirectToAction("MyOrder", new { id = data.CustomerID });

        }

      

    }
}