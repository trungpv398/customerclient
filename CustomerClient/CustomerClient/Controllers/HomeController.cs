﻿using CustomerClient.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomerClient.Controllers
{
    public class HomeController : Controller
    {
        private Category category;
        private ProductAuction productAuction;
        private Customer customer;
        private CustomerSuport customerSuport;
        private UserManuals userManuals;
        private Feedback feedback;
        private Banner banner;
        public HomeController()
        {
            category        = new Category();
            productAuction  = new ProductAuction();
            customer        = new Customer();
            customerSuport  = new CustomerSuport();
            userManuals     = new UserManuals();
            feedback        = new Feedback();
            banner          = new Banner();
        }
        public ActionResult Index()
        {
            UpdateStPro();
            ViewBag.Category = category.GetAll().Where(x => x.Status == 1);
            ViewBag.ProAuAll = productAuction.GetByStatus(1).OrderByDescending(x=>x.Created).Take(10);
            ViewBag.ProAuSt0 = productAuction.GetByStatus(0).OrderByDescending(x=>x.Created).Take(10);
            ViewBag.Banner = banner.GetBannerByTypeBaner(1);

            ViewBag.ProTake3EndTm = productAuction.GetProByEndTimeDESC();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            var pwd = customer.GETSHA(form["password"]);
            var data = customer.GetAll();
            foreach(var item in data)
            {
                if(item.Email == form["email"])
                {
                    if(item.Password == pwd)
                    {
                        Customer c = item;
                        Session["loginCustomer"] = c;
                        TempData["Success"] = "Successfully login..!";
                        return RedirectToAction("Index");
                    }
                   
                }               
            }
            TempData["errors"] = "Your account is incorrect..!";
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(FormCollection form)
        {
            try
            {
                Customer cs = new Customer();
                cs.Created = DateTime.Now;
                cs.Email = form["email"];
                cs.FullName = form["fullname"];
                cs.Password = form["password"];
                cs.PhoneNumber = form["phonenumber"];
                cs.Status = 1;
                cs.Address = form["address"];
                cs.CMND = form["cmnd"];
                var data = customer.AddNew(cs);
                if(data.StatusCode==200)
                {
                    TempData["Success"] = "Successfully register..!";
                    return RedirectToAction("Login");
                }
                else
                {
                    TempData["errors"] = "Register failed..!";
                    return RedirectToAction("Login");
                }
            }
            catch { }
            TempData["errors"] = "Register failed..!";
            return RedirectToAction("Register");
        }

        public ActionResult Logout()
        {
            Session["loginCustomer"] = null;
            TempData["Success"] = "Successfully logout..!";
            return RedirectToAction("Index");
        }

        public ActionResult Contacts()
        {
            return View();
        }

        public ActionResult UserManuals(int id)
        {
            var data = userManuals.GetById(id);
            ViewBag.Data = data;
            return View();
        }
        public ActionResult Support(int id)
        {
            var data = customerSuport.GetById(id);
            ViewBag.Data = data;
            return View();
        }

        public ActionResult Feedback()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Feedback(FormCollection form)
        {
               

           
                Feedback fb = new Feedback();
                fb.Created = DateTime.Now;
                fb.Status = 0;
                fb.UserName = form["username"];
                fb.Email = form["email"];
                fb.Content = form["message"];
            feedback.AddNew(fb);
                TempData["Success"] = "Successfully Feedback..!";

            return View();

        }

        //cap nhat trang thai truoc khi vao
        public void UpdateStPro()
        {
            //cap nhat cac san pham sap dien ra
            productAuction.UpProStUpComming(0,1);
            //cap nhat cac san pham da het gio
            productAuction.UpProStWork(1,2);
        }
       
    }
}