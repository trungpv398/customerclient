﻿using CustomerClient.Common;
using CustomerClient.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomerClient.Controllers
{
    public class ProductAuctionController : Controller
    {
        ProductAuction productAuction;
        Auction auction;
        ProAuParameter proAuParameter;
        Brand brand;
        private Order order;
        private Customer customer;
        private Category category;
        public ProductAuctionController()
        {
            productAuction  = new ProductAuction();
            auction         = new Auction();
            proAuParameter  = new ProAuParameter();
            brand           = new Brand();
            order           = new Order();
            customer        = new Customer();
            category        = new Category();
        }
        // GET: ProductAuction
        public ActionResult GetAll(int? page,int? sort_name,int? sort_pro)
        {
            int _page = page ?? 1;
            int _pageSize = 20;
            int _sort_name = sort_name ?? 0;
            int _sort_pro = sort_pro ?? 0;
            var dataAllSt1 = productAuction.GetAllProAuc();
           
            switch (_sort_name)
            {
                case 0:
                    ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                    break;
                case 1:
                    switch(sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                            break;
                    }
                    break;                    
                case 2:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.ProName).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.ProName).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.ProName).ToPagedList(_page, _pageSize);
                            break;
                    }
                   
                    break;
                case 3:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);
                            break;
                    }
                 
                    break;
                case 4:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.StartDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.StartDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.StartDateTime).ToPagedList(_page, _pageSize);
                            break;
                    }

                    break;
                default:
                    ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                    break;
            }




            var dataCate = category.GetAll().Where(x => x.Status == 1);
            ViewBag.Category = dataCate;

            ViewBag.ProTake3EndTm = productAuction.GetProByEndTimeDESC();
            return View();
        }

        //Get pro by Category
        public ActionResult GetProByCate(int cateid,int? page,int? sort_name,int? sort_pro)
        {
            int _page = page ?? 1;
            int _pageSize = 20;
            int _sort_name = sort_name ?? 0;
            int _sort_pro = sort_pro ?? 0;


            var dataAllSt1 = productAuction.GetAllProAuc().Where(x=>x.CategoryID == cateid).Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status);
            switch (_sort_name)
            {
                case 0:
                    ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                    break;
                case 1:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                            break;
                    }
                    break;
                case 2:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.ProName).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.ProName).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.ProName).ToPagedList(_page, _pageSize);
                            break;
                    }

                    break;
                case 3:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);
                            break;
                    }

                    break;
                case 4:
                    switch (sort_pro)
                    {
                        case 0:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.StartDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 1:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1).OrderByDescending(x => x.StartDateTime).ToPagedList(_page, _pageSize);
                            break;
                        case 2:
                            ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 0).OrderByDescending(x => x.StartDateTime).ToPagedList(_page, _pageSize);
                            break;
                    }

                    break;
                default:
                    ViewBag.DataAllSt = dataAllSt1.Where(x => x.Status == 1 || x.Status == 0).OrderByDescending(x => x.Status).ToPagedList(_page, _pageSize);
                    break;
            }

            ViewBag.CateId = cateid;
            ViewBag.ProTake3EndTm = productAuction.GetProByEndTimeDESC();
            return View();
        }
        public ActionResult Details(int id)
        {
            var data = productAuction.GetById(id);
            ViewBag.DetailProAuction = data;
            ViewBag.Brand = brand.GetById(data.BrandID);
            var dataAuction = auction.GetByProduct(id);
            if(dataAuction.Count()==0)
            {
                ViewBag.ProAuction = null;
            }
            else
            {
                ViewBag.ProAuction = dataAuction.OrderByDescending(x => x.Created).First();
                

            }
            var dataParamPro = proAuParameter.GetProAu(id);
            //lay cac phien dau gia cua san pham
            var dataAuctionAll = auction.GetList(id).OrderByDescending(x => x.Created);
            ViewBag.Parameter = dataParamPro;
            ViewBag.ProAuctionAll = dataAuctionAll;
            //dem so phien dau gia
            ViewBag.TotalAution = dataAuctionAll.Count();
            return View();
        }

        [HttpPost]
        public ActionResult AddAution(FormCollection form)
        {
            ProductAuction proData = productAuction.GetById(int.Parse(form["proId"]));
            if(DateTime.Now >= proData.EndDateTime)
            {
                TempData["errors"] = "The auction has ended..!";
                return RedirectToAction("Index", "Home");
            }
            var dataAution = auction.AucPriceDESC(int.Parse(form["proId"]));
            if (dataAution != null)
            {
                if (float.Parse(form["price"]) < dataAution.Price+proData.PriceStep)
                {
                    TempData["errors"] = "The bet amount cannot be less than and equal to the current price..!";
                    return RedirectToAction("Details", new { id = int.Parse(form["proId"]) });
                }
            }
            try
            {
                Auction a = new Auction();
                a.Created = DateTime.Now;
                a.CustomerID = int.Parse(form["custId"]);
                a.ProAutionID = int.Parse(form["proId"]);
                a.Price = float.Parse(form["price"]);
                auction.AddNew(a);

                TempData["Success"] = "You have successfully bid..!";
                return RedirectToAction("Details", new { id = int.Parse(form["proId"]) });
            }
            catch { }
            TempData["errors"] = "The auction has ended..!";
            return RedirectToAction("Index", "Home");
        }



        //Tao don hang cho khách hàng mua ngay san phẩm
        [HttpPost]
        public ActionResult BuyNow(FormCollection form)
        {
            Order odNew = new Order();
            Auction auNew = new Auction();
            Customer custData = customer.GetById(int.Parse(form["custome_id"]));
           /* custData.Password = customer.GETSHA(custData.Password);
            var b = custData.Password;*/
            auNew.ProAutionID = int.Parse(form["pro_id"]);
            auNew.Created = DateTime.Now;
            auNew.CustomerID = int.Parse(form["custome_id"]);
            auNew.Price = float.Parse(form["total_price"]);
            auNew.Status = 5;

            var dataAuction = auction.AddNew(auNew);

            var aucfee = (float.Parse(form["total_price"]) * 5) / 100;
            var moneyRece = float.Parse(form["total_price"]) - aucfee;
            odNew.Created = DateTime.Now;
            odNew.status = 1;
            odNew.AutionID = dataAuction.ID;
            odNew.EndDateTime = DateTime.Now;
            odNew.Receiver = form["receiver"];
            odNew.CustomerID = int.Parse(form["custome_id"]);
            odNew.Payment = int.Parse(form["payment"]);
            odNew.Email = form["email"];
            odNew.Note = form["note"];
            odNew.Phone = form["phone"];
            odNew.TotalPrice = float.Parse(form["total_price"]);
            odNew.AuctionFees = aucfee;
            odNew.AuctionFees = moneyRece;
            odNew.ProAutionID = int.Parse(form["pro_id"]);
            var dataProAu = productAuction.GetById(int.Parse(form["pro_id"]));
            var data = order.AddNew(odNew);
            
            if(data.StatusCode==200)
            {
                dataProAu.Status = 5;
                productAuction.Edit(dataProAu);
                
                custData.Rate = custData.Rate + 5;
                customer.Edit(custData);
                //mail
                string content = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Template/neworder.html"));
                content = content.Replace("{{Image}}", dataProAu.Image);
                content = content.Replace("{{ProName}}", dataProAu.ProName);
                content = content.Replace("{{ProCode}}",Convert.ToString(dataProAu.ID));
                content = content.Replace("{{Email}}", odNew.Email);
                content = content.Replace("{{Phone}}", order.Phone);
                content = content.Replace("{{Note}}", Convert.ToString(odNew.ID));
                content = content.Replace("{{Fullname}}", odNew.Receiver);
                content = content.Replace("{{pathToFile}}", "https://res.cloudinary.com/dk65dthn5/image/upload/v1611217961/logo2_lidt9c.png");
                content = content.Replace("{{Total}}", odNew.TotalPrice.ToString("N0"));
                var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

                new MailHelper().SendMail(odNew.Email, "Đơn hàng mới từ OnlineShop", content);
                new MailHelper().SendMail(toEmail, "Đơn hàng mới từ OnlineShop", content);

                TempData["Success"] = "Successful purchase..!";
                return RedirectToAction("Index","Home");
            }

            TempData["errors"] = "Purchase failed..!";
            return RedirectToAction("Details",new { id = int.Parse(form["pro_id"]) });
        }
    }
}