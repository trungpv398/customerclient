﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Auction
    {
        public int ID { get; set; }
        public int ProAutionID { get; set; }
        public int CustomerID { get; set; }
        public float Price { get; set; }
        public int? Status { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("ProAutionID")]
        public ProductAuction ProductAuction { get; set; }
        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        public IEnumerable<Order> Orders { get; set; }


        public IEnumerable<Auction> GetAll()
        {
            IEnumerable<Auction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Auction>>().Result;
            return data;

        }
        public Auction GetById(int id)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction/" + id).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public IEnumerable<Auction> GetByProduct(int proAuctionID)
        {
            IEnumerable<Auction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction?proAuctionID=" + proAuctionID).Result;
            data = response.Content.ReadAsAsync<IEnumerable<Auction>>().Result;
            return data;
        }
        public Auction Edit(Auction c)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Auction", c).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction AddNew(Auction c)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Auction", c).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction Delete(int id)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Auction/" + id).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction AucPriceDESC(int proAuctionID)
        {
            var data = GetByProduct(proAuctionID);

            if (data.Count() > 0)
            {
                return data.OrderByDescending(x => x.Created).First();
            }
            else
            {
                return null;
            }
        }
        public List<Auction> GetList(int id)
        {
            Customer c = new Customer();
            ProductAuction p = new ProductAuction();
            List<Auction> list = new List<Auction>();
            var data = GetByProduct(id);
            
            foreach(var item in data)
            {
                item.Customer = c.GetById(item.CustomerID);
                item.ProductAuction = p.GetById(item.ProAutionID);
                list.Add(item);
            }
            return list;
        }

        //get Aution by Customer Id

        public List<Auction> GetListByCustomer(int cusId)
        {
            var data = GetAll();
            return data.Where(x=>x.CustomerID==cusId).ToList();
        }

        public List<Auction> GetListByCustId(int cust_id)
        {
            var data = GetAll().Where(x=>x.CustomerID == cust_id);
            ProductAuction productAuction = new ProductAuction();
            Customer customer = new Customer();
            List<Auction> list = new List<Auction>();
            foreach(var item in data)
            {
                item.ProductAuction = productAuction.GetById(item.ProAutionID);
                item.Customer = customer.GetById(item.CustomerID);
                list.Add(item);
            }

            return list;
        }

        //get Aution by id
        public Auction GetByAucId(int id)
        {
            Customer customer = new Customer();
            ProductAuction pro = new ProductAuction();
            var data = GetById(id);
            data.Customer = customer.GetCustById(data.CustomerID);
            data.ProductAuction = pro.GetById(data.ProAutionID);
            return data;
        }
        //status
        public string AuctionStatus(int status)
        {
            var value = "";
            switch(status)
            {
                case 0:
                    value = "Đang chờ";
                    break;
                case 1:
                    value = "chiến thắng";
                    break;
                case 2:
                    value = "Thất bại";
                    break;
                case 3:
                    value = "chiến thắng";
                    break;
                case 4:
                    value = "chiến thắng";
                    break;
                case 5:
                    value = "Buy now";
                    break;
                default:
                    value = "Đang chờ";
                    break;
            }
            return value;
        }
        public string AuctionStatusClass(int status)
        {
            
            var value = "";
            switch (status)
            {
                case 0:
                    value = "badge badge-danger";
                    break;
                case 1:
                    value = "badge badge-success";
                    break;
                case 2:
                    value = "badge badge-danger";
                    break;
                case 3:
                    value = "badge badge-success";
                    break;
                case 4:
                    value = "badge badge-success";
                    break;
                case 5:
                    value = "badge badge-success";
                    break;
                default:
                    value = "badge badge-primary";
                    break;
            }
            return value;
        }


        //Count all Auc by proID
        public int CountAucByProId(int id)
        {
            var data = GetByProduct(id);
            return data.Count();
        }
    }
}