﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class ProductAuction
    {

        public int ID { get; set; }
        public string ProName { get; set; }
        // noi san xuat
        public string Origin { get; set; }
        //gia goc
        public float OriginalPrice { get; set; }
        //gia buoc nhay
        public float PriceStep { get; set; }
        //gia mua ngay
        public float PurchasingPrice { get; set; }
        //gia khoi diem
        public float StartingPrice { get; set; }
        //mo ta san pham
        public string Description { get; set; }
        public string Image { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Qty { get; set; }
        public int Status { get; set; }
        public int CategoryID { get; set; }
        public int SaleManID { get; set; }
        public int BrandID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        [ForeignKey("CategoryID")]
        public Category Category { get; set; }
        [ForeignKey("SaleManID")]
        public SalesMan SalesMan { get; set; }
        [ForeignKey("BrandID")]
        public Brand Brand { get; set; }
        public IEnumerable<ProAuParameter> ProAuParameters { get; set; }
        public IEnumerable<Action> Actions { get; set; }

        public IEnumerable<ProductAuction> GetAll()
        {
            IEnumerable<ProductAuction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction").Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProductAuction>>().Result;
            return data;

        }
        public IEnumerable<ProductAuction> GetByStatus(int status)
        {
            IEnumerable<ProductAuction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction?status="+status).Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProductAuction>>().Result;
            return data;

        }
        public ProductAuction Edit(ProductAuction c)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("ProductAuction", c).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }
        public ProductAuction GetById(int id)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction/" + id).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }

        //get ra all product
        public List<ProductAuction> GetAllProAuc()
        {
            var data = GetAll();
            Category category = new Category();
            Brand brand = new Brand();
            SalesMan salesMan = new SalesMan();
            List<ProductAuction> list = new List<ProductAuction>();
            foreach(var item in data)
            {
                item.Category = category.GetById(item.CategoryID);
                item.Brand = brand.GetById(item.BrandID);
                item.SalesMan = salesMan.GetById(item.SaleManID);
                list.Add(item);
            }
            
            return list;
        }

        //Get 3 product enddatetime
        public List<ProductAuction> GetProByEndTimeDESC()
        {
            var data = GetAllProAuc().Where(x => x.Status == 1).OrderBy(x=>x.EndDateTime).Take(3).ToList();
            return data;
        }
            


        public void UpProStUpComming(int st,int stNew)
        {
            var time = DateTime.Now;
            var data = GetAll().Where(x => x.Status == st);
            foreach (var item in data)
            {
                if (item.StartDateTime <= time)
                {
                    UpdateStatus(item.ID,stNew);
                }
            }
        }
        public void UpProStWork(int st, int stNew)
        {
            var time = DateTime.Now;
            var data = GetAll().Where(x => x.Status == st);
            foreach (var item in data)
            {
                if (item.EndDateTime <= time)
                {
                    UpdateStatus(item.ID, stNew);
                }
            }
        }
        public void UpdateStatus(int proId,int stNew)
        {
            var data = GetById(proId);
            data.Status = stNew;
            Edit(data);
        }

    }
}