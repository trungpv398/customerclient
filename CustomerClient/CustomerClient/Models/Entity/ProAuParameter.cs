﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class ProAuParameter
    {
        public int ID { get; set; }
        public int? ProAuctionID { get; set; }
        public int ParameterID { get; set; }
        public string ParameterName { get; set; }
        public string Value { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("ProAuctionID")]
        public ProductAuction ProductAuction { get; set; }
        /* [ForeignKey("ParameterID")]*/
        /*public Parameter Parameter { get; set; }*/

        public IEnumerable<ProAuParameter> GetAll()
        {
            IEnumerable<ProAuParameter> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProAu_Parameter").Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProAuParameter>>().Result;
            return data;

        }

        public ProAuParameter GetById(int id)
        {
            ProAuParameter data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProAu_Parameter/" + id).Result;
            data = response.Content.ReadAsAsync<ProAuParameter>().Result;
            return data;
        }

        public IEnumerable<ProAuParameter> GetProAu(int proAuID)
        {

            IEnumerable<ProAuParameter> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProAu_Parameter?proAuctionId="+proAuID).Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProAuParameter>>().Result;
            return data;
        }

    }
}