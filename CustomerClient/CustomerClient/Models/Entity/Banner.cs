﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Banner
    {
        public int ID { get; set; }
        public int TypeBannerID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }

        public IEnumerable<Banner> GetAll()
        {
            IEnumerable<Banner> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Banner").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Banner>>().Result;
            return data;

        }

        public Banner GetById(int id)
        {
            Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Banner/" + id).Result;
            data = response.Content.ReadAsAsync<Banner>().Result;
            return data;
        }

        public Banner GetBannerByTypeBaner(int type_id)
        {
            var data = GetAll().Where(x => x.TypeBannerID == type_id).OrderBy(x => x.Created).First();

            return data;
        }

    }
}