﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Order
    {
        public int ID { get; set; }
        public int AutionID { get; set; }
        //ten nguoi nhan
        public string Receiver { get; set; }
        public int CustomerID { get; set; }
        public int Payment { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public int status { get; set; }
        public string Phone { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("AutionID")]
        public Auction Auction { get; set; }
        public float AuctionFees { get; set; }
        public float MoneyReceived { get; set; }
        public float TotalPrice { get; set; }
        public int? ProAutionID { get; set; }
        public IEnumerable<Order> GetAll()
        {
            IEnumerable<Order> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Order").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Order>>().Result;
            return data;

        }
        public Message AddNew(Order c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Order", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public Order GetById(int id)
        {
            Order data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Order/" + id).Result;
            data = response.Content.ReadAsAsync<Order>().Result;
            return data;
        }
        public Order Edit(Order c)
        {
            Order data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Order", c).Result;
            data = response.Content.ReadAsAsync<Order>().Result;
            return data;
        }

        public List<Order> GetAllOrder()
        {
            Auction au = new Auction();
            var data = GetAll();
            List<Order> list = new List<Order>();
            foreach(var item in data)
            {
                item.Auction = au.GetByAucId(item.AutionID);
                list.Add(item);
            }
            

            return list;
        }
        public List<Order> GetOdByCustID(int id)
        {
            var data = GetAllOrder();
            return data.Where(x=>x.CustomerID==id).ToList();
        }

        //get Order By Id
        public Order GetOrderById(int id)
        {
            Auction auction = new Auction();

            var order = GetById(id);
            order.Auction = auction.GetByAucId(order.AutionID);

            return order;
        }


        //status don hang
        public string OrderSt(int status)
        {
            var value = "";
            switch (status)
            {
                case 0:
                    //dang cho kh xac nhan
                    value = "Wait for confirmation";
                    break;
                case 1:
                    //dang cho kh giao hang
                    value = "Waiting for delivery";
                    break;
                case 2:
                    //Dang giao hang
                    value = "Being transported";
                    break;
                case 3:
                    //Da giao hang
                    value = "Complete";
                    break;
                case 4:
                    //bi huy
                    value = "Cancel";
                    break;
                default:
                    value = "chua xac dinh";
                    break;
            }
            return value;
        }
        //class status don hang
        public string OrderStClass(int status)
        {
            var value = "";
            switch (status)
            {
                case 0:
                    //dang cho kh xac nhan
                    value = "badge badge-secondary";
                    break;
                case 1:
                    //dang cho kh giao hang
                    value = "badge badge-warning";
                    break;
                case 2:
                    //Dang giao hang
                    value = "badge badge-success";
                    break;
                case 3:
                    //Da giao hang
                    value = "badge badge-info";
                    break;
                case 4:
                    //bi huy
                    value = "badge badge-danger";
                    break;
                default:
                    value = "badge badge-primary";
                    break;
            }
            return value;
        }
    }
}