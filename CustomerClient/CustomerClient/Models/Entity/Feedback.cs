﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Feedback
    {
        public int ID { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Content { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public Feedback AddNew(Feedback c)
        {
            Feedback data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("FeedBack", c).Result;
            data = response.Content.ReadAsAsync<Feedback>().Result;
            return data;
        }
    }
}