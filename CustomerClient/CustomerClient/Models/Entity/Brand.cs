﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Brand
    {
        public int ID { get; set; }
        public string BrandName { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public int TypeProID { get; set; }
        public DateTime Created { get; set; }
        /* [ForeignKey("TypeProID")]
         public Type_Product Type_Product { get; set; }*/
        public IEnumerable<Brand> GetAll()
        {
            IEnumerable<Brand> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Brand").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Brand>>().Result;
            return data;

        }

        public Brand GetById(int id)
        {
            Brand data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Brand/" + id).Result;
            data = response.Content.ReadAsAsync<Brand>().Result;
            return data;
        }


    }
}