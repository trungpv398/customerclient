﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class CustomerSuport
    {

        public int ID { get; set; }
        [Required(ErrorMessage = "Khong duoc de trong title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Khong duoc de trong content")]
        public string Content { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }

        public IEnumerable<CustomerSuport> GetAll()
        {
            IEnumerable<CustomerSuport> supList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Support").Result;
            supList = response.Content.ReadAsAsync<IEnumerable<CustomerSuport>>().Result;
            return supList;

        }

        public CustomerSuport GetById(int id)
        {
            CustomerSuport cusSup;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Support/" + id).Result;
            cusSup = response.Content.ReadAsAsync<CustomerSuport>().Result;
            return cusSup;
        }

    }
}