﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Message
    {
        public int StatusCode { get; set; }
        public string Messages { get; set; }
        public int Id { get; set; }
    }
}