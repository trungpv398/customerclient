﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Customer
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public string CMND { get; set; }
        public string PhoneNumber { get; set; }
        public string Avatar { get; set; }
        public int? Rate { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Auction> Auctions { get; set; }
        public IEnumerable<Customer> GetAll()
        {
            IEnumerable<Customer> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Customer").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
            return data;

        }

        public Customer GetById(int id)
        {
            Customer data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Customer/" + id).Result;
            data = response.Content.ReadAsAsync<Customer>().Result;
            return data;
        }

        public Customer Edit(Customer c)
        {
            Customer data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Customer", c).Result;
            data = response.Content.ReadAsAsync<Customer>().Result;
            return data;
        }

        public Message AddNew(Customer c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Customer", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }

        public Customer GetCustById(int id)
        {
            var customer = GetById(id);
            Auction au = new Auction();
            var dataAu = au.GetListByCustomer(id);
            customer.Auctions = dataAu;
            return customer;
        }


        //ma hoa pass
        public string GETSHA(string str)
        {
            /*MD5 md5 = new MD5CryptoServiceProvider();*/
            SHA512 sha = new SHA512CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = sha.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }


    }
}