﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class SalesMan
    {

        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public string CMND { get; set; }
        public string Avatar { get; set; }
        public int? Rate { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<ProductAuction> ProductAuctions { get; set; }
        public IEnumerable<SalesMan> GetAll()
        {
            IEnumerable<SalesMan> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("SalesMan").Result;
            data = response.Content.ReadAsAsync<IEnumerable<SalesMan>>().Result;
            return data;

        }

        public SalesMan GetById(int id)
        {
            SalesMan data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("SalesMan/" + id).Result;
            data = response.Content.ReadAsAsync<SalesMan>().Result;
            return data;
        }

        public SalesMan Edit(SalesMan c)
        {
            SalesMan data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("SalesMan", c).Result;
            data = response.Content.ReadAsAsync<SalesMan>().Result;
            return data;
        }
    }
}