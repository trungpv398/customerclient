﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class Category
    {
        public int ID { get; set; }
        public string CateName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<ProductAuction> ProductAuctions { get; set; }


        public IEnumerable<Category> GetAll()
        {
            IEnumerable<Category> cateList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Category").Result;
            cateList = response.Content.ReadAsAsync<IEnumerable<Category>>().Result;
            return cateList;

        }

        public Category GetById(int id)
        {
            Category category;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Category/" + id).Result;
            category = response.Content.ReadAsAsync<Category>().Result;
            return category;
        }


    }
}