﻿using CustomerClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CustomerClient.Models.Entity
{
    public class UserManuals
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }

        public IEnumerable<UserManuals> GetAll()
        {
            IEnumerable<UserManuals> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("UserManuals").Result;
            data = response.Content.ReadAsAsync<IEnumerable<UserManuals>>().Result;
            return data;

        }

        public UserManuals GetById(int id)
        {
            UserManuals data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("UserManuals/" + id).Result;
            data = response.Content.ReadAsAsync<UserManuals>().Result;
            return data;
        }
    }
}